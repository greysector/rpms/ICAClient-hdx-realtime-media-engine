%global debug_package %{nil}

Name:           ICAClient-hdx-realtime-media-engine
Version:        2.2
Release:        3
Summary:        HDX RealTime Media Engine for Microsoft Skype™ for Business and Lync®

License:        Citrix
URL:            https://www.citrix.com/downloads/citrix-receiver/additional-client-software/hdx-realtime-media-engine-22.html
Source0:        https://downloads.citrix.com/11922/HDX_RealTime_Media_Engine_%{version}_for_Linux_x64.zip

BuildArch:      x86_64
BuildRequires:  execstack
Requires:       libdbus-glib-1.so.2()(64bit)
Requires:       libglib-2.0.so.0()(64bit)
Requires:       libgobject-2.0.so.0()(64bit)
Requires:       libudev.so.1()(64bit)
Requires:       libusb-1.0.so.0()(64bit)
Requires:       ICAClient%{_isa}

%global rpmver %{version}.0-837

%description
The HDX RealTime Media Engine is a plug-in to the Citrix Receiver to support
clear, crisp high-definition audio-video calls, particularly with Microsoft
Skype™ for Business and Microsoft Lync®. Users can seamlessly participate in
audio-video or audio-only calls to and from other HDX RealTime users, native
Skype for Business and Lync client users, and other standards-based desktop
video and conference room systems. The HDX RealTime Media Engine is available
for Windows, Mac and Linux devices.

HDX RealTime Media Engine 2.x supports Microsoft Skype® for Business Server
2015, Microsoft Lync® Server 2013 and Office 365™ (Skype® for Business
Online).

%prep
%setup -q -c
rpm2cpio HDX_RealTime_Media_Engine_%{version}_for_Linux_x64/x86_64/citrix-hdx-realtime-media-engine-%{rpmver}.x86_64.rpm | \
    cpio --extract --make-directories --no-absolute-filenames --preserve-modification-time
pushd usr/local/bin
execstack -c RTMediaEngineSRV
chmod -x 50-hid.rules *.wav EULA.rtf
popd

%build

%install
install -d %{buildroot}{%{_libdir}/ICAClient/rtme,/var/{lib,log}/RTMediaEngineSRV}
touch %{buildroot}/var/lib/RTMediaEngineSRV/settings.conf
pushd usr/local/bin
install -pm755 RTMEconfig RTMediaEngineSRV %{buildroot}%{_libdir}/ICAClient/rtme
install -pm755 HDXRTME.so %{buildroot}%{_libdir}/ICAClient
install -pm644 DialTone_US.wav InboundCallRing.wav %{buildroot}%{_libdir}/ICAClient/rtme
popd

%posttrans
pushd %{_libdir}/ICAClient/nls/en >/dev/null
%{_libdir}/ICAClient/rtme/RTMEconfig -install && mv new_module.ini module.ini
popd >/dev/null
exit 0

%preun
pushd %{_libdir}/ICAClient/nls/en >/dev/null
%{_libdir}/ICAClient/rtme/RTMEconfig -remove && mv new_module.ini module.ini
popd >/dev/null
exit 0

%triggerin -- ICAClient
pushd %{_libdir}/ICAClient/nls/en >/dev/null
%{_libdir}/ICAClient/rtme/RTMEconfig -install && mv new_module.ini module.ini
popd >/dev/null
exit 0

%files
%license usr/local/bin/EULA.rtf
%{_libdir}/ICAClient/HDXRTME.so
%{_libdir}/ICAClient/rtme
%attr(1777,root,root) %dir /var/lib/RTMediaEngineSRV
%ghost /var/lib/RTMediaEngineSRV/settings.conf
%attr(1777,root,root) %dir /var/log/RTMediaEngineSRV

%changelog
* Wed Jan 10 2018 Dominik Mierzejewski <rpm@greysector.net> 2.2-3
- update system config when ICAClient is updated

* Mon Nov 27 2017 Dominik Mierzejewski <rpm@greysector.net> 2.2-2
- make /var/lib/RTMediaEngineSRV world writable and sticky
- drop i686 version

* Thu Mar 09 2017 Dominik Mierzejewski <rpm@greysector.net> 2.2-1
- update to 2.2

* Fri Dec 09 2016 Dominik Mierzejewski <rpm@greysector.net> 2.1.200-1
- update to 2.1.200

* Fri Jul 29 2016 Dominik Mierzejewski <rpm@greysector.net> 2.1-1
- update to 2.1
- include x86_64 version

* Fri Feb 19 2016 Dominik Mierzejewski <rpm@greysector.net> 2.0.100-1
- repackage upstream RPMs
